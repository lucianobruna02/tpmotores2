using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pin : MonoBehaviour
{
    private bool isPinned = false;

    public float speed = 20f;
    public Rigidbody rb;

    private void Update()
    {
        if(!isPinned)
        rb.MovePosition(rb.position + Vector3.right * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Rotator")
        {
            transform.SetParent(col.transform);
            col.GetComponent<Rotator>().speed += 10f;
            //col.GetComponent<Rotator>().speed *= -1f;
            isPinned = true;
        }
        else if(col.tag == "Pin")
        {
            SceneManager.LoadScene(0);
        }

    }
}
