using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdObstacles : MonoBehaviour
{
    public float sendTimer = 1;
    public float frequency = 2;
    public float position;
    public GameObject myObstacle;
    public GameObject mainCharacter;

    void Update()
    {
        sendTimer -= Time.deltaTime;
        if (sendTimer <= 0)
        {
            position = Random.Range(1.03f, -4.73f);
            transform.position = new Vector3(9.272231f, position, -0.1970615f);
            Instantiate(myObstacle, transform.position, transform.rotation);
            sendTimer = frequency;
        }

        if (mainCharacter != null) Time.timeScale = 1;
        else Time.timeScale = 0;
    }
}
