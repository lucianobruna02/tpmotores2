using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdBounce : MonoBehaviour
{
    public float ySpeed;
    public float yTarget;
    public GameObject SoundBounce;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, ySpeed, 0);
        ySpeed = Mathf.Lerp(ySpeed, yTarget, .025f);
        if (Input.GetKeyDown("space"))
        {
            Instantiate(SoundBounce, transform.position, transform.rotation);
            ySpeed = 0.065f;
        }
    }
}
