using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BirdCollision : MonoBehaviour
{
    public GameObject deathExplosion;
    public GameObject checkpointSound;
    void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.tag == "Eviroment")
        {
            GameObject deathObj = Instantiate(deathExplosion, transform.position, transform.rotation) as GameObject;
            Destroy(gameObject);
            SceneManager.LoadScene(1);
           
        }
        else if(other.gameObject.tag == "CheckPoint")
        {
            Instantiate(checkpointSound, transform.position, transform.rotation);
        }
    }

   
}
